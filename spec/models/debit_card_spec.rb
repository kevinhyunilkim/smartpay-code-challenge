require 'rails_helper'

RSpec.describe DebitCard, type: :model do
  describe '#card_number' do
    it 'is required' do
      debit_card = FactoryGirl.build(:debit_card)
      debit_card.card_number = nil
      debit_card.valid?
      expect(debit_card.errors[:card_number].size).to eq(1)
    end

    it 'is unique with a combination of expiration date' do
      debit_card = FactoryGirl.create(:debit_card)
      another_debit_card = FactoryGirl.build(:debit_card)
      another_debit_card.card_number = debit_card.card_number
      another_debit_card.expiration_date = debit_card.expiration_date
      another_debit_card.valid?
      expect(another_debit_card.errors[:card_number].size).to eq(1)
    end

    context 'when the card number is less than 16-digit' do
      it 'is invalid' do
        debit_card = FactoryGirl.build(:debit_card)
        debit_card.card_number = '424242424242424'
        debit_card.valid?
        expect(debit_card.errors[:card_number].size).to eq(1)
      end
    end

    context 'when the card number is greather than 16-digit' do
      it 'is invalid' do
        debit_card = FactoryGirl.build(:debit_card)
        debit_card.card_number = '42424242424242424'
        debit_card.valid?
        expect(debit_card.errors[:card_number].size).to eq(1)
      end
    end

    context 'when the card number contains non-numerical characters' do
      it 'is invalid' do
        debit_card = FactoryGirl.build(:debit_card)
        debit_card.card_number = 'abcd424242424242'
        debit_card.valid?
        expect(debit_card.errors[:card_number].size).to eq(1)
      end
    end
  end

  describe '#expiration_date' do
    it 'is required' do
      debit_card = FactoryGirl.build(:debit_card)
      debit_card.expiration_date = nil
      debit_card.valid?
      expect(debit_card.errors[:expiration_date].size).to eq(1)
    end

    context 'when the expiration date is in the past' do
      it 'is invalid' do
        debit_card = FactoryGirl.build(:debit_card)
        debit_card.expiration_date = Date.current.last_month.end_of_month
        debit_card.valid?
        expect(debit_card.errors[:expiration_date].size).to eq(1)
      end
    end
  end

  describe '#cvv' do
    it 'is required' do
      debit_card = FactoryGirl.build(:debit_card)
      debit_card.cvv = nil
      debit_card.valid?
      expect(debit_card.errors[:cvv].size).to eq(1)
    end
  end

  describe '#user' do
    it 'can only be used by only one customer' do
      debit_card = FactoryGirl.create(:debit_card)
      another_debit_card = FactoryGirl.build(:debit_card)
      another_debit_card.user = debit_card.user
      another_debit_card.valid?
      expect(another_debit_card.errors[:user].size).to eq(1)
    end
  end
end
