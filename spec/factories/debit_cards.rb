FactoryGirl.define do
  factory :debit_card do
    user
    card_number '4242424242424242'
    expiration_date { Date.current.next_year.end_of_month }
    cvv '123'
  end
end
