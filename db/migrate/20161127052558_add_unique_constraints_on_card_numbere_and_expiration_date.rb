class AddUniqueConstraintsOnCardNumbereAndExpirationDate < ActiveRecord::Migration
  def change
    add_index :debit_cards, [:card_number, :expiration_date], unique: true
  end
end
