class CreateDebitCards < ActiveRecord::Migration
  def change
    create_table :debit_cards do |t|
      t.references :user, index: true, foreign_key: true
      t.string :card_number, null: false
      t.date :expiration_date, null: false
      t.string :cvv, null: false

      t.timestamps null: false
    end
  end
end
