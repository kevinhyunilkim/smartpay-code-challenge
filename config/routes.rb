Rails.application.routes.draw do
  resource :debit_card, only: [:new, :update]

  root to: 'debit_cards#new'
end
