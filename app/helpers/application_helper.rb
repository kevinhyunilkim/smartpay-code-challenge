module ApplicationHelper
  def expiration_date_to_human(expiration_date)
    expiration_date.strftime('%m/%Y')
  end

  def card_number_to_human(card_number)
    card_number.scan(/.{1,4}/).join(' ')
  end
end
