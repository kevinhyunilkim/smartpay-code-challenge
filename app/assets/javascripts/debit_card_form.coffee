$ ->
  class DebitCardForm
    constructor: ->
      $('input[name="debit_card_form[payment_card]"]').change @paymentCardChanged
      @setupOutlets()
      @activatePaymentInputs()
      @activatePreviewInputEvents()
      @previewCard()
      @form.submit @submit

    paymentCard: ->
      $('input[name="debit_card_form[payment_card]"]:checked').val()

    paymentCardChanged: =>
      @clearInputErrors()
      @activatePreviewInputEvents()
      @previewCard()

    setupOutlets: ->
      @form = $('#edit_debit_card_form')
      @cardNumberInput = $('#debit_card_form_new_card_attributes_card_number')
      @expMonthInput = $('#debit_card_form_new_card_attributes_expiration_date_2i')
      @expYearInput = $('#debit_card_form_new_card_attributes_expiration_date_1i')
      @cvvInput = $('#debit_card_form_new_card_attributes_cvv')

    activatePaymentInputs: ->
      @cardNumberInput.payment('formatCardNumber')
      @cvvInput.payment('formatCardCVC')

    activatePreviewInputEvents: ->
      if @paymentCard() is 'existing_card'
        @cardNumberInput.off 'keyup'
        @expMonthInput.off 'change'
        @expYearInput.off 'change'
      else
        @cardNumberInput.on 'keyup', @previewCard
        @expMonthInput.on 'change', @previewCard
        @expYearInput.on 'change', @previewCard
        @cardNumberInput.focus()

    previewCard: =>
      $previewCardWrapper = $('#preview-card')

      if @paymentCard() is 'existing_card'
        existingCardInfo = $('[data-behavior~=existing-card-info]')
        cardNumber = existingCardInfo.data('card-number')
        expMonth = existingCardInfo.data('exp-month')
        expYear = existingCardInfo.data('exp-year')
        fullName = existingCardInfo.data('full-name')
        $previewCardWrapper.find('.debit-card').addClass('existing-card')
      else
        cardNumber = @cardNumberInput.val() or '•••• •••• •••• ••••'
        expMonth = @expMonthInput.val() or '••'
        expYear = @expYearInput.val() or '••'
        fullName = $('#cardholder-name').text() or 'Full Name'
        $previewCardWrapper.find('.debit-card').removeClass('existing-card')

      $previewCardWrapper.find('.card-number').text(cardNumber)
      $previewCardWrapper.find('.card-expiry').text("#{expMonth}/#{expYear}")
      $previewCardWrapper.find('.card-name').text(fullName)

    clearInputErrors: ->
      @form.find('.form-group').removeClass('has-danger')
      @form.find('.form-control-feedback').remove()

    toggleInputError: ($element, errored, message) ->
      $formGroup = $element.parents('.form-group')
      $formGroup.toggleClass('has-danger', errored)
      $formGroup.find('.form-control-feedback').remove()

      if errored
        @errored = errored
        $formGroup.append("<div class='form-control-feedback'>#{message}</div>")

    validatePaymentInputs: ->
      @errored = false
      @toggleInputError @cardNumberInput, !$.payment.validateCardNumber(@cardNumberInput.val()), 'Must be a valid card number.'
      @toggleInputError @expMonthInput, !$.payment.validateCardExpiry(@expMonthInput.val(), @expYearInput.val()), 'Must be in the future.'
      @toggleInputError @cvvInput, !$.payment.validateCardCVC(@cvvInput.val()), 'Must be 3 or 4 digits numeric.'
      @errored

    submit: (e) =>
      if @paymentCard() is 'new_card'
        e.preventDefault() if @validatePaymentInputs()

  new DebitCardForm()
