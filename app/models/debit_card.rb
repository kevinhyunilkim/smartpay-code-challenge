class DebitCard < ActiveRecord::Base
  belongs_to :user

  before_validation :normalize_card_number

  validates :user, uniqueness: true
  validates :card_number, presence: true
  validates :card_number, uniqueness: { scope: :expiration_date }
  validates :card_number, numericality: { only_integer: true }, allow_blank: true
  validates :card_number, length: { is: 16 }, allow_blank: true
  validates :expiration_date, presence: true
  validates :expiration_date, date: { after_or_equal_to: Proc.new { Date.current.beginning_of_month } }, allow_blank: true
  validates :cvv, presence: true
  validates :cvv, numericality: { only_integer: true }, allow_blank: true
  validates :cvv, length: { in: 3..4 }, allow_blank: true

  def last_4_digits
    card_number&.last(4)
  end

  def stub_perform_payment(amount)
    card_number == '4242424242424242' ? '100' : '203'
  end

  private

  def normalize_card_number
    card_number&.delete!(' ')
  end
end
