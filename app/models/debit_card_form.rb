class DebitCardForm
  include ActiveModel::Model

  EXISTING_CARD = 'existing_card'.freeze
  NEW_CARD = 'new_card'.freeze

  attr_accessor :user, :new_card, :payment_card

  validates :existing_card, presence: true, if: -> { payment_card == EXISTING_CARD }

  def new_card_attributes=(attributes)
    @new_card = DebitCard.new(attributes)
  end

  def new_card
    @new_card ||= DebitCard.new
  end

  def existing_card
    @existing_card ||= user.debit_card
  end

  def payment_card
    @payment_card ||= existing_card ? EXISTING_CARD : NEW_CARD
  end

  def save
    case payment_card
    when EXISTING_CARD
      valid?
    when NEW_CARD
      new_card.valid? && !!(user.debit_card = new_card) # Replace with the new card
    else
      false
    end
  end

  def persisted?
    true
  end
end
