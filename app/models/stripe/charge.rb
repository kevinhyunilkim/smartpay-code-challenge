module Stripe
  class Charge
    def self.create(card:, amount:)
      case card.stub_perform_payment(amount)
      when '100'
        'Your order has been completed!'
      when '203'
        'Your payment has been declined. Please re-check with the providing bank.'
      else
        'Unexpected error has occurred. Please contact Kevin at (574) 347-5571.'
      end
    end
  end
end
