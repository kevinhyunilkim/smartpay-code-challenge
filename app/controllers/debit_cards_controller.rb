class DebitCardsController < ApplicationController
  def new
    @debit_card_form = DebitCardForm.new(user: current_user)
  end

  def update
    @debit_card_form = DebitCardForm.new(debit_card_form_params.merge(user: current_user))

    if @debit_card_form.save
      message = Stripe::Charge.create(card: current_user.debit_card, amount: 10_000)

      redirect_to new_debit_card_url, notice: message
    else
      render :new
    end
  end

  private

  def debit_card_form_params
    params.require(:debit_card_form).permit(:payment_card, new_card_attributes: [:card_number, :expiration_date, :cvv])
  end

  def existing_card_present?
    @debit_card_form.existing_card.present?
  end

  helper_method :existing_card_present?
end
