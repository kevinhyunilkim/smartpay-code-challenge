class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_user
    @current_user ||= User.find_or_create_by(first_name: 'Kevin', last_name: 'Kim')
  end

  helper_method :current_user
end
